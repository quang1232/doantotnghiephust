import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'contact_controller.dart';
import 'main.dart';

class ListContact extends GetView<ContactController>{
  final contactController = Get.put(ContactController());

  ListContact({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text("Danh bạ"),
        backgroundColor: Colors.green,
        leading: IconButton(onPressed: () {
          Get.delete<ContactController>();
          Get.back();
        }, icon: const Icon(
            Icons.arrow_back
        )),
      ),
      body: SpeechSampleApp(
        body: Obx(() => Column(
          children: [
            Expanded(child:  ListView.builder(
              physics: const AlwaysScrollableScrollPhysics(),
              itemCount: contactController.contacts.length,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return Container(
                  margin: const EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                  padding: const EdgeInsets.symmetric(horizontal: 12,vertical: 4),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.green),
                      borderRadius: BorderRadius.circular(12)
                  ),
                  child: Row(
                    children: [
                      Text("${contactController.contacts[index].displayName}",style: const TextStyle(color: Colors.black),),
                      const SizedBox(width: 8,),
                      controller.contacts[index].phones!.isNotEmpty ?Text("${contactController.contacts[index].phones![0].value}", style: const TextStyle(color: Colors.black)):Container()
                    ],
                  ),
                );
              },),)
          ],
        )),
      ),
    ), onWillPop: () async{
      Get.delete<ContactController>();
      Get.back();
      return false;
    },);
  }

}